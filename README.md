
Greetings and thanks for the opportunity.

This app was developed using MVVM patter, separating **Model**, **Views** / **Viewcontrollers** and finally **ViewModels** to manage URL requests and parse the results in order to return them to the **viewControllers**.

This app uses cocoapods. I used **Moya + Alamofire + ReactiveSwift** libraries to perform service calls in a more elegant and convenient way. Initially I started calling the services with the basic `URLSession.shared.dataTask` but from my experience with medium to big projects, is easier to understand, configure, handle and extend requests using this structure.

I used the `Main.storyboard` as well as separate views with their own `.xib` files.

Apologies for the lack of documentation and unit tests, I focused on the main features and ended up sacrificing both.

To clone the project :  **git clone  https://bitbucket.org/cghost182/wundertest.git**

---

## Running the project

In order to run the **wunderTest** project, please read this notes:


1. Xcode version 10.1.
2. Deployment target was set to 9.0.
3. Swift version used was 4.2.
4. This project uses cocoapods, so please open the .xcworkspace file to run the project .
5. The pods folder was uploaded to the repository, so the project should run directly.
6. If there are problems building the project, please execute  **pod install** in the terminal.
7. I edited the `Podfile` to force all the pods to use swift 4.2 version and avoid compilation errors. If swift version errors are prompted please enter to the **Pods** project, select the affected targets and in **Buil Settings** find **Swift Language Version**, change it to 4.2.

---

## Possible performance optimizations

1. Avoid using the Main Storyboard , I prefer separate views in Xib's files. In that way I avoid an overload of the storyboard file ( we know it would be highly slow to open and work in huge storyboards ).
2. Cache the selected car data and image to avoid calling the services several times if the item is clicked twice.
---

## Improvements (what could be done better)

1.  Handle all the possible response failures ( nil or unexisting values or wrong data types ) at the momento of parse.
2.  Damage description in details view should had used a bigger cell height or other type of cell ( xib ) to show larger information.
3.  Add failure callback in image loading.
4.  Add failure view if rental process returns error.
5.  Add more animations or transitions to display views / elements.
5.  Add better documentation.
6.  Implement more unit tests.
