//
//  DetailsTableViewCell.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(withTitle title : String , value : Any?){
        self.titleLbl.text = title
        
        switch value {
        case is Int:
            let intValue = (value as? Int) ?? -1
            self.valueLbl.text = intValue != -1 ? String(intValue) : ""
        case is Double:
            let doubleValue = (value as? Double) ?? -1
            self.valueLbl.text = doubleValue != -1 ? String(doubleValue) : ""
        case is Bool:
            let boolValue = (value as? Bool) ?? false
            self.valueLbl.text = boolValue ? "Yes" : "No"
        default:
            self.valueLbl.text = value as? String ?? ""
        }
        
    }
    
}
