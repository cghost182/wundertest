//
//  RentalConfirmationView.swift
//  wundertest
//
//  Created by Christian Collazos on 9/8/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import UIKit

protocol RentalConfirmationDelegate : class {
    func returnToMainView()
}

class RentalConfirmationView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var checkIcon: UIImageView!
    
    weak var delegate : RentalConfirmationDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit(){
        Bundle.main.loadNibNamed("RentalConfirmationView", owner: self, options: nil)
        addSubview(contentView)
        
        contentView.frame = self.bounds
        contentView.layer.cornerRadius = 10
        closeBtn.layer.cornerRadius = 20
        checkIcon.tintColor = .green
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        contentView.removeFromSuperview()
        delegate?.returnToMainView()
    }
    
}
