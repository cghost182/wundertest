//
//  CalloutView.swift
//  wundertest
//
//  Created by Christian Collazos on 9/6/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import UIKit


class CalloutView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }

    private func customInit(){
        Bundle.main.loadNibNamed("CalloutView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
}
