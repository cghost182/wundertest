//
//  VehiclePinItem.swift
//  wundertest
//
//  Created by Christian Collazos on 9/5/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import MapKit

class VehiclePinItem: MKPointAnnotation {
    
    @objc var carId : Int = -1
    @objc var icon: UIImage?
    @objc var angle: Double = 0
    @objc var vehicleTitle: String? = ""
    
    init(vehicle: Vehicle) {
        super.init()
        
        coordinate = CLLocationCoordinate2DMake(vehicle.lat, vehicle.lon)
        carId = vehicle.carId
        icon = UIImage(named: "car_icon")!
        angle = Double(arc4random_uniform(UInt32(500))) * (Double.pi / 180)
        vehicleTitle = vehicle.title
    }
}
