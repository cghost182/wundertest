//
//  UIImageViewExtension.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func load(url: URL, onSuccess success: @escaping () -> Void) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        success()
                    }
                }
            }
        }
    }
}
