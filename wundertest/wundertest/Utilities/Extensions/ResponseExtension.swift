//
//  ResponseExtension.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import Moya
import ReactiveSwift

public extension Response {
    
    /// Maps response data into a model object implementing the Decodable protocol.
    /// Throws a JSONMapping error on failure.
    public func mapJSONObject<T: Decodable>(_ type: T.Type) throws -> T {
        do{
            let result = try JSONDecoder().decode(type, from: self.data)
            return result
        }catch{
            throw MoyaError.jsonMapping(self)
        }
    }
    
    /// Maps the response data into an array of model objects implementing the Decodable protocol.
    /// Throws a JSONMapping error on failure.
    public func mapJSONArray<T: Decodable>(_ type: T.Type) throws -> [T] {
        do{
            let result = try JSONDecoder().decode([T].self, from: self.data)
            return result
        }catch{
            throw MoyaError.jsonMapping(self)
        }
    }
    
}

/// Extension for processing raw NSData generated by network access.
extension SignalProducerProtocol where Value == Response, Error == MoyaError {
    
    /// Maps data received from the signal into a JSON object. If the conversion fails, the signal errors.
    public func mapJSONObject<T: Decodable>(_ type: T.Type) -> SignalProducer<T, MoyaError> {
        return producer.flatMap(.latest) { response -> SignalProducer<T, MoyaError> in
            return unwrapThrowable { try response.mapJSONObject(type) }
        }
    }
    
    /// Maps data received from the signal into a JSON Array object. If the conversion fails, the signal errors.
    public func mapJSONArray<T: Decodable>(_ type: T.Type) -> SignalProducer<[T], MoyaError> {
        return producer.flatMap(.latest) { response -> SignalProducer<[T], MoyaError> in
            return unwrapThrowable { try response.mapJSONArray(type) }
        }
    }
    
}

/// Maps throwable to SignalProducer.
private func unwrapThrowable<T>(throwable: () throws -> T) -> SignalProducer<T, MoyaError> {
    do {
        return SignalProducer(value: try throwable())
    } catch {
        if let error = error as? MoyaError {
            return SignalProducer(error: error)
        } else {
            // The cast above should never fail, but just in case.
            return SignalProducer(error: MoyaError.underlying(error as NSError, nil))
        }
    }
}

