//
//  Detail.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation

struct DetailItem {
    public private(set) var title : String
    public private(set) var value : Any?
    
    init(title : String, value : Any?){
        self.title = title
        self.value = value
    }
}

struct Detail : Codable {
    
    var carId: Int
    var title: String?
    var lat: Double
    var lon: Double
    var licencePlate: String
    var fuelLevel: Int
    var vehicleStateId: Int
    var vehicleTypeId:Int
    var pricingTime: String
    var pricingParking: String
    var reservationState: Int
    var isClean: Bool
    var isDamaged: Bool
    var address: String
    var zipCode: String
    var city: String
    var locationId: Int
    var hardwareId: String
    var isActivatedByHardware : Bool
    var damageDescription: String
    var vehicleTypeImageUrl : String
    
    enum CodingKeys: String , CodingKey {
        case carId
        case title
        case lat
        case lon
        case licencePlate
        case fuelLevel
        case vehicleStateId
        case vehicleTypeId
        case pricingTime
        case pricingParking
        case reservationState
        case isClean
        case isDamaged
        case address
        case zipCode
        case city
        case locationId
        case hardwareId
        case isActivatedByHardware
        case damageDescription
        case vehicleTypeImageUrl
    }
    
}
