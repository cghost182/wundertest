//
//  Vehicle.swift
//  wundertest
//
//  Created by Christian Collazos on 9/4/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation

class Vehicle : NSObject, Codable {

    var carId: Int
    var title: String?
    var lat: Double
    var lon: Double
    var licencePlate: String
    var fuelLevel: Int
    var vehicleStateId: Int
    var vehicleTypeId:Int
    var pricingTime: String
    var pricingParking: String
    var reservationState: Int
    var isClean: Bool
    var isDamaged: Bool
    var distance: String
    var address: String
    var zipCode: String
    var city: String
    var locationId: Int
  
    enum CodingKeys: String , CodingKey {
        case carId
        case title
        case lat
        case lon
        case licencePlate
        case fuelLevel
        case vehicleStateId
        case vehicleTypeId
        case pricingTime
        case pricingParking
        case reservationState
        case isClean
        case isDamaged
        case distance
        case address
        case zipCode
        case city
        case locationId
    }
    
    override init() {
        carId = 0
        title = ""
        lat = 0.0
        lon = 0.0
        licencePlate = ""
        fuelLevel = 0
        vehicleStateId = 0
        vehicleTypeId = 0
        pricingTime = ""
        pricingParking = ""
        reservationState = 0
        isClean = true
        isDamaged = false
        distance = ""
        address = ""
        zipCode = ""
        city = ""
        locationId = 0
    }
    
}
