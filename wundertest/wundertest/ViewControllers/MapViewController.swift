//
//  MapViewController.swift
//  wundertest
//
//  Created by Christian Collazos on 9/4/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController , CLLocationManagerDelegate, MKMapViewDelegate {
    
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var resetBtnHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    let locationManager = CLLocationManager()
    var viewModel: MapViewModel!
    var calloutView = CalloutView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = MapViewModel(delegate: self)
        
        //Location Manager configuration
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        //View configuration
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        resetBtn.isHidden = true
        resetBtn.layer.cornerRadius = 20
        resetBtnHeightConstraint.constant = 0
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Select a car"
        resetMapView()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    func resetMapView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.resetBtnHeightConstraint.constant = 0
            self.view.layoutIfNeeded()
        }) { (finished : Bool) in
            self.resetBtn.isHidden = true
        }
        
        calloutView.removeFromSuperview()
        self.mapView.removeAnnotations(mapView.annotations)
        viewModel.getVehicles()
    }
    
    // MARK: - Actions
    @IBAction func resetAction(_ sender: Any) {
        resetMapView()
    }
    
}

// MARK: - Extensions

extension MapViewController : MapViewDelegate {
    func addPinsToMap(_ annotations: [MKAnnotation]) {
        
        DispatchQueue.main.async {
            self.mapView.removeAnnotations(self.mapView.annotations)
            self.mapView.addAnnotations(annotations)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let pinIdentifier = self.viewModel.pinIdentifier
        var pinView: MKAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdentifier) ?? nil
        if  pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: pinIdentifier)
            
            let imageView = UIImageView()
            imageView.tag = 111;
            imageView.contentMode = .center
            pinView!.addSubview(imageView)
            
        } else {
            pinView!.annotation = annotation
        }
        
        
        let pointAnnotation = annotation as! VehiclePinItem
        let imageView: UIImageView = pinView!.viewWithTag(111) as! UIImageView
        imageView.image = pointAnnotation.icon
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(pointAnnotation.angle))
        
        return pinView;
    }
    
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if view.annotation is MKUserLocation
        {
            return
        }
        
        if let selectedAnnotation = view.annotation as? VehiclePinItem {
            
            mapView.deselectAnnotation(selectedAnnotation, animated: false)
            
            if self.mapView.annotations.count == 2 {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let detailsViewController = storyBoard.instantiateViewController(withIdentifier: "detailsViewController") as! DetailsViewController
                detailsViewController.configure(withCarId: selectedAnnotation.carId)
                self.navigationItem.title = ""
                self.navigationController?.pushViewController(detailsViewController, animated: true)
                
            }else{               
                
                resetBtn.isHidden = false
                
                UIView.animate(withDuration: 0.3) {
                    self.resetBtnHeightConstraint.constant = 50
                    self.view.layoutIfNeeded()
                }
                
                DispatchQueue.main.async {
                    
                    self.calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -self.calloutView.bounds.size.height*0.52)
                    self.calloutView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
                    if let vehicleTitle = selectedAnnotation.vehicleTitle , vehicleTitle != "" {
                        self.calloutView.titleLbl.text = selectedAnnotation.vehicleTitle
                    }else{
                        self.calloutView.titleLbl.text =  "Awesome car"
                    }
                    
                    view.addSubview(self.calloutView)
                    mapView.setCenter((selectedAnnotation.coordinate), animated: true)
                    
                    let itemsToRemove = self.mapView.annotations.filter{
                        return $0.hash != view.annotation?.hash
                    }
                    
                    self.mapView.removeAnnotations(itemsToRemove)
                }
                
            }
        }
        
    }
    
    
}


