//
//  DetailsViewController.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var carDetailsTable: UITableView!
    @IBOutlet weak var rentBtn: UIButton!
    @IBOutlet weak var imageActivityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    
    private var carId = 0
    private var detailsViewModel :DetailsViewModel!
    private var tableDataValues = [DetailItem]()
    private var rentalConfirmationView : RentalConfirmationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carDetailsTable.delegate = self
        carDetailsTable.dataSource = self
        carDetailsTable.register(DetailsTableViewCell.nib, forCellReuseIdentifier: DetailsTableViewCell.identifier)
        rentBtn.layer.cornerRadius = 20
        rentBtn.setTitle("Quick rent!", for: .normal)
        
        rentalConfirmationView = RentalConfirmationView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        rentalConfirmationView.delegate = self
        
        detailsViewModel.getVehicleDetails(vehicleId: carId)
    }
    
    func configure( withCarId carId : Int){
        self.carId = carId
        detailsViewModel = DetailsViewModel(delegate: self)
    }
    
    @IBAction func reserveAction(_ sender: Any) {
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        rentBtn.setTitle("Requesting vehicle ... ", for: .normal)
        detailsViewModel.rentVehicle(vehicleId: carId)
    }
}

extension DetailsViewController : DetailsViewModelDelegate{
    func presentCarDetails(details: [DetailItem]) {
        tableDataValues = details.filter{$0.title != "details"}
        self.imageActivityIndicatorView.startAnimating()
        
        if let details = details[0].value as? Detail , let carImgURL = URL(string: details.vehicleTypeImageUrl){
            
            self.carImg.load(url: carImgURL, onSuccess: { () in
                self.imageActivityIndicatorView.stopAnimating()
            })
            carDetailsTable.reloadData()
        }
    }
    
    func rentalPerformed() {
        activityIndicatorView.stopAnimating()
        rentBtn.setTitle("Done!", for: .normal)
        view.addSubview(self.rentalConfirmationView)
    }
    
    func errorGettingData() {
        self.imageActivityIndicatorView.stopAnimating()
        let alert = UIAlertController(title: "Error :(", message: "Oops! something failed getting this vehicle data", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Take me back", style: .default, handler: { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        self.present(alert, animated: true)
    }
    
}

extension DetailsViewController : RentalConfirmationDelegate{
    func returnToMainView() {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension DetailsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableDataValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: DetailsTableViewCell.identifier) as! DetailsTableViewCell
        detailsCell.configure(withTitle: tableDataValues[indexPath.row].title, value: tableDataValues[indexPath.row].value)
        return detailsCell
        
    }
    
    
}

