//
//  MapViewModel.swift
//  wundertest
//
//  Created by Christian Collazos on 9/5/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import MapKit
import Moya
import ReactiveSwift

@objc protocol MapViewDelegate {
    func addPinsToMap(_ annotations: [MKAnnotation])
}

class MapViewModel : NSObject {
    
    let pinIdentifier = "annotation"
    private weak var delegate: MapViewDelegate?
    
    
    // MARK: - Initialization
    @objc init(delegate: MapViewDelegate? = nil) {
        super.init()
        
        self.delegate = delegate
    }
    
    func getVehicles () {

        DataServiceAPI.reactive.request(.RequestVehicles).filterSuccessfulStatusCodes().mapJSONObject([Vehicle].self).start { event in
            switch event {
            case let .value(vehicles):
                self.setVehiclesPins(vehicles)
            case let .failed(error):
                print(error.localizedDescription)
            default:
                break
            }
        }
        
    }
    
    private func setVehiclesPins(_ vehicles: [Vehicle]) {
        DispatchQueue.main.async {
            var annotations = [MKAnnotation]()
            vehicles.forEach {
                let annotationView = self.createVehiclePin(vehicle: $0)
                annotations.append(annotationView.annotation!)
            }
            
            self.delegate?.addPinsToMap(annotations)
        }
    }
    
    private func createVehiclePin(vehicle: Vehicle) -> MKPinAnnotationView {
        let vehicleAnnotation = VehiclePinItem(vehicle: vehicle)
        return MKPinAnnotationView(annotation: vehicleAnnotation, reuseIdentifier: pinIdentifier)
    }
}

