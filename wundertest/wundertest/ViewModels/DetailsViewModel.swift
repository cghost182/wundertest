//
//  DetailsViewModel.swift
//  wundertest
//
//  Created by Christian Collazos on 9/7/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import MapKit
import Moya
import ReactiveSwift

protocol DetailsViewModelDelegate : class {
    func presentCarDetails(details : [DetailItem])
    func rentalPerformed()
    func errorGettingData()
}

class DetailsViewModel : NSObject {
    
    private weak var delegate: DetailsViewModelDelegate?
    
    // MARK: - Initialization
    init(delegate: DetailsViewModelDelegate? = nil) {
        super.init()
        self.delegate = delegate
    }
    
    
    func getVehicleDetails( vehicleId : Int){
        DataServiceAPI.reactive.request(.GetDetails(vehicleId)).filterSuccessfulStatusCodes().mapJSONObject(Detail.self).start { event in
            switch event {
            case let .value(details):
                self.delegate?.presentCarDetails(details: self.getDetailsArray(details : details))
            case let .failed(error):
                self.delegate?.errorGettingData()
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
    
    func rentVehicle( vehicleId : Int){
        DataServiceAPI.reactive.request(.RentSelectedCar(vehicleId)).filterSuccessfulStatusCodes().mapJSON().start { event in
            switch event {
            case .value(_):
                self.delegate?.rentalPerformed()
            case let .failed(error):
                print(error.localizedDescription)
            default:
                break
            }
        }
    }
    
    private func getDetailsArray(details : Detail) -> [DetailItem] {
        
        var detailsArray = [DetailItem]()
        
        detailsArray.append(DetailItem(title: "details", value: details))
        detailsArray.append(DetailItem(title: "Title", value: details.title))
        detailsArray.append(DetailItem(title: "Is it clean?", value: details.isClean))
        detailsArray.append(DetailItem(title: "Is it damaged?", value: details.isDamaged))
        detailsArray.append(DetailItem(title: "Licence plate", value: details.licencePlate))
        detailsArray.append(DetailItem(title: "Fuel level", value: details.fuelLevel))
        detailsArray.append(DetailItem(title: "Hardware", value: details.hardwareId))
        detailsArray.append(DetailItem(title: "Pricing time", value: details.pricingTime))
        detailsArray.append(DetailItem(title: "Pricing parking", value: details.pricingParking))
        detailsArray.append(DetailItem(title: "Activated by hardware?", value: details.isActivatedByHardware))
        detailsArray.append(DetailItem(title: "Address", value: details.address))
        detailsArray.append(DetailItem(title: "Zip code", value: details.zipCode))
        detailsArray.append(DetailItem(title: "City", value: details.city))
        detailsArray.append(DetailItem(title: "Latitude", value: details.lat))
        detailsArray.append(DetailItem(title: "Longitude", value: details.lon))
        detailsArray.append(DetailItem(title: "Damage description", value: details.damageDescription))
        
        return detailsArray
    }
    

}

