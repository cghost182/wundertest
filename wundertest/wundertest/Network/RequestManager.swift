//
//  RequestManager.swift
//  wundertest
//
//  Created by Christian Collazos on 9/4/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
import Alamofire
import Moya
import ReactiveSwift

let DataServiceAPI = MoyaProvider<RequestManager>(endpointClosure: endpointClosure)

enum RequestManager {
    case RequestVehicles
    case GetDetails(Int)
    case RentSelectedCar(Int)
}

// MARK: - TargetType Protocol Implementation
extension RequestManager: TargetType {
    var baseURL: URL { return URL(string: "https://s3.eu-central-1.amazonaws.com")! }
    var postBaseURL : URL {return URL(string: "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com")!}
    
    var path: String {
        switch self {
        case .RequestVehicles:
            return "/wunderfleet-recruiting-dev/cars.json"
            
        case .GetDetails(let carId):
            return "/wunderfleet-recruiting-dev/cars/\(carId)"
            
        case .RentSelectedCar:
            return "/default/wunderfleet-recruiting-mobile-dev-quick-rental"
        }
        
    }
    var method: Moya.Method {
        switch self {
        case .RentSelectedCar:
            return .post
        default:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .RequestVehicles,
             .GetDetails:
            
            return .requestPlain
            
        case .RentSelectedCar(let carId):
            return .requestCompositeParameters(bodyParameters: ["carId": carId], bodyEncoding: JSONEncoding.default, urlParameters: [:])
            
        }
    }
    var sampleData: Data {
        return Data()
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
    
}


let endpointClosure = { (target: RequestManager) -> Endpoint in
    
    switch target {
        
    case .RentSelectedCar:
        let url = target.postBaseURL.appendingPathComponent(target.path).absoluteString
        let endpoint: Endpoint = Endpoint(url: url, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, task: target.task, httpHeaderFields: target.headers)

        return endpoint.adding(newHTTPHeaderFields: ["Authorization": "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa"])
    
    default:
        let url = target.baseURL.appendingPathComponent(target.path).absoluteString
        let endpoint: Endpoint = Endpoint(url: url, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: target.method, task: target.task, httpHeaderFields: target.headers)

        return endpoint
    }
}
