//
//  MapViewDelegateMock.swift
//  wundertestTests
//
//  Created by Christian Collazos on 9/8/19.
//  Copyright © 2019 Bizagi. All rights reserved.
//

import Foundation
@testable import wundertest
import MapKit

class MapViewDelegateMock: MapViewDelegate {
    
    var addPinsToMap : ((_ annotations: [MKAnnotation]) -> Void)?
    
    func addPinsToMap(_ annotations: [MKAnnotation]){
        addPinsToMap?(annotations)
    }
    
}

