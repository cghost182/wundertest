//
//  VehicleMock.swift
//  wundertestTests
//
//  Created by Christian Collazos on 9/8/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import Foundation
@testable import wundertest

class VehicleMock : Vehicle {
    func setMockInfo() {
        carId = 1234
        lat = 51.511998
        lon = 7.462532
        title = "Awesome car"
    }
    
}
