//
//  wundertestTests.swift
//  wundertestTests
//
//  Created by Christian Collazos on 9/4/19.
//  Copyright © 2019 Christian Collazos. All rights reserved.
//

import XCTest
@testable import wundertest

class wundertestTests: XCTestCase {

    var viewModel : MapViewModel!
    var vehicleMock = VehicleMock()
    var mapViewDelegateMock = MapViewDelegateMock()

    override func setUp() {
        viewModel = MapViewModel()
        vehicleMock.setMockInfo()
    }
    
    func testVehicleData() {
        
        XCTAssertEqual(vehicleMock.carId, 1234, "car Id invalid")
        XCTAssertNotNil(vehicleMock.lon, "longitude should exist")
        XCTAssertNotNil(vehicleMock.lat, "lon should exist")
    }
    
    func testRequestingVehiclesData(){
        let expectation = self.expectation(description: "annotations obtained")
        
        mapViewDelegateMock.addPinsToMap = { annotations in
            expectation.fulfill()
            
            XCTAssertFalse(annotations.isEmpty, "annotations list should not be empty")
            
            for annotation in annotations {
                XCTAssertNotNil(annotation.coordinate)
            }
        }
        viewModel.getVehicles()
        waitForExpectations(timeout: 10, handler: nil)
    }

}
